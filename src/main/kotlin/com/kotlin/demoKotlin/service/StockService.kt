package com.kotlin.demoKotlin.service

import com.kotlin.demoKotlin.gen.model.StockData
import com.kotlin.demoKotlin.gen.model.StockUpdateRequestBody
import com.kotlin.demoKotlin.model.Stock
import com.kotlin.demoKotlin.model.StockDto
import com.kotlin.demoKotlin.repository.StockRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

@Service
class StockService {
    lateinit var stockRepository: StockRepository
    fun createStock(stockDto: StockDto): StockData {
        val res = stockRepository.save(Stock(null, stockDto.isin, stockDto.name, stockDto.currentPrice))
        return StockData(res.id, res.isin, res.name, res.currentPrice, res.lastUpdate)
    }

    fun deleteStock(id: Long): Unit {
        stockRepository.deleteById(id)
    }

    fun fetchAllStocks(page: Int, size: Int): Page<StockData> {
        return stockRepository.findAll(Pageable.ofSize(size).withPage(page)).map { res -> StockData(res.id, res.isin, res.name, res.currentPrice, res.lastUpdate) }
    }

    fun fetchStockById(id: Long): StockData {
        val res = stockRepository.findById(id).orElseThrow()
        return StockData(res.id, res.isin, res.name, res.currentPrice, res.lastUpdate)
    }

    fun updateStock(id: Long, stockUpdateRequestBody: StockUpdateRequestBody): StockData {
        val res = stockRepository.findById(id).orElseThrow()
        res.currentPrice = stockUpdateRequestBody.currentPrice
        res.lastUpdate = OffsetDateTime.now()
        stockRepository.save(res)
        return StockData(res.id, res.isin, res.name, res.currentPrice, res.lastUpdate)
    }
}
package com.kotlin.demoKotlin.repository

import com.kotlin.demoKotlin.model.Stock
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface StockRepository : JpaRepository<Stock, Long>{
   /* fun createStock(stockRequestBody: StockRequestBody): ResponseEntity<StockResponse> {
        return super.createStock(stockRequestBody)
    }

    fun deleteStock(id: Long): ResponseEntity<Unit> {
        return super.deleteStock(id)
    }

    fun fetchAllStocks(page: Int?, size: Int?): ResponseEntity<StockArrayResponse> {
        return super.fetchAllStocks(page, size)
    }

    fun fetchStockById(id: Long): ResponseEntity<StockResponse> {
        return super.fetchStockById(id)
    }

    fun updateStock(id: Long, stockUpdateRequestBody: StockUpdateRequestBody): ResponseEntity<StockResponse> {
        return super.updateStock(id, stockUpdateRequestBody)
    }*/
}
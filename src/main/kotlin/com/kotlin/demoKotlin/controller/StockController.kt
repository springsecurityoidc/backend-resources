package com.kotlin.demoKotlin.controller

import com.kotlin.demoKotlin.gen.api.StockControllerApi
import com.kotlin.demoKotlin.gen.model.StockArrayResponse
import com.kotlin.demoKotlin.gen.model.StockRequestBody
import com.kotlin.demoKotlin.gen.model.StockResponse
import com.kotlin.demoKotlin.gen.model.StockUpdateRequestBody
import com.kotlin.demoKotlin.model.StockDto
import com.kotlin.demoKotlin.service.StockService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity


class StockController: StockControllerApi{
    lateinit var stockService:StockService
    override fun createStock(stockRequestBody: StockRequestBody): ResponseEntity<StockResponse> {
        return ResponseEntity.ok(StockResponse(HttpStatus.OK.name, stockService.createStock(StockDto(stockRequestBody.name, stockRequestBody.isin, stockRequestBody.currentPrice))))
    }

    override fun deleteStock(id: Long): ResponseEntity<Unit> {
        stockService.deleteStock(id)
        return ResponseEntity.ok().build()
    }

    override fun fetchAllStocks(page: Int?, size: Int?): ResponseEntity<StockArrayResponse> {
        val res = stockService.fetchAllStocks(page?:0, size?:10)
        return ResponseEntity.ok(StockArrayResponse(HttpStatus.OK.name, res.content, res.number, res.totalPages, res.totalElements.toInt()))
    }

    override fun fetchStockById(id: Long): ResponseEntity<StockResponse> {
        return ResponseEntity.ok(StockResponse(HttpStatus.OK.name, stockService.fetchStockById(id)))
    }

    override fun updateStock(id: Long, stockUpdateRequestBody: StockUpdateRequestBody): ResponseEntity<StockResponse> {
        return ResponseEntity.ok(StockResponse(HttpStatus.OK.name, stockService.updateStock(id, stockUpdateRequestBody)))
    }
}
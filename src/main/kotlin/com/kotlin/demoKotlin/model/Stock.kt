package com.kotlin.demoKotlin.model

import jakarta.persistence.*
import java.time.OffsetDateTime

@Entity
class Stock(id: Long?, name: String?, isin: String?, currentPrice: Double?) {
    constructor() : this(null, null, null, null)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null

    @Column(name = "name", nullable = false)
    var name: String? = null

    @Column(name = "isin", nullable = false)
    var isin: String? = null

    @Column(name = "current_price", nullable = false)
    var currentPrice: Double = 0.0

    @Column(name = "last_update")
    var lastUpdate: OffsetDateTime? = null
}
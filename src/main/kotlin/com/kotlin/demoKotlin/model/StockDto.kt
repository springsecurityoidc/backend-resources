package com.kotlin.demoKotlin.model

data class StockDto(val name: String, val isin: String, val currentPrice: Double)